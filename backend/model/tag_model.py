from elasticsearch import Elasticsearch, helpers
from util import Util

class TagModel():
    def __init__(self):
        self.ut = Util()
        self.es = Elasticsearch()

    def create(self, tag_object):
        res = self.es.index(index="test-index", doc_type='tweet', id=1, body=tag_object)
        return res["result"]

    def fetch(self, email_ids):
        result_obj = helpers.scan(self.es, query = self.ut.filter_by_email_id(email_ids), index="index_tag")
        results = []
        for res in result_obj:
            results.append({
                "id": res["_id"],
                "word": res["_source"]["doc"]["word"],
                "tag": res["_source"]["doc"]["tag"]
            })

        return results