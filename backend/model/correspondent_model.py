from elasticsearch import Elasticsearch, helpers
from util import Util

class CorrespondentModel():
    def __init__(self):
        self.ut = Util()
        self.es = Elasticsearch()

    def fetch(self, email_ids):
        result_obj = helpers.scan(self.es, query = self.ut.filter_by_email_id(email_ids),
                                 index="index_correspondent")
        results = []
        for res in result_obj:
            results.append({
                "id": res["_id"],
                "email": res["_source"]["doc"]["email"],
                "sent": res["_source"]["doc"]["sent"],
                "received": res["_source"]["doc"]["received"]
            })

        return results