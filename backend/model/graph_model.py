from elasticsearch import Elasticsearch, helpers
class GraphModel():
    def __init__(self):
        self.es = Elasticsearch()

    def query_params_for_es(self, email_ids):
        query = []
        for email_id in email_ids:
            query.append({ "match": { "_id": email_id } })

    def filter_by_email_id(self, email_ids):
        return {
          "query": {
            "bool": {
              "must" : {
                "bool" : {
                  "should" : self.query_params_for_es(email_ids)
                }
              }
            }
          }
        }

    def query_params_for_es(self, email_ids):
        query = []
        for email_id in email_ids:
            query.append({ "match": { "_id": email_id } })

    def filter_by_email_id(self, email_ids):
        return {
          "query": {
            "bool": {
              "must" : {
                "bool" : {
                  "should" : self.query_params_for_es(email_ids)
                }
              }
            }
          }
        }

    def fetch(self, email_ids):
        result_obj = helpers.scan(self.es, query = self.filter_by_email_id(email_ids), index="index_graph")
        results = []
        for res in result_obj:
            results.append({
                "id": res["_id"],
                "from": res["_source"]["doc"]["from"],
                "to": res["_source"]["doc"]["to"],
                "numberOfEmails": res["_source"]["doc"]["numberOfEmails"]
            })

        return results