from elasticsearch import Elasticsearch, helpers

class EmailModel():
    def __init__(self):
        self.logger = open("../logs/logger.log", "a")
        self.es = Elasticsearch()

    def gendata(self, json_docs):
        index = 1
        for doc in json_docs:
            yield {
                '_op_type': 'create',
                "_index": "index_email",
                "_type": "email_doc",
                "_id": index,
                "doc": {
                    "subject": doc["Subject"],
                    "content": doc["content"],
                    "date": doc.get("Date")
                }
            }
            index += 1

    def create(self, email_objects):
        self.logger.write("[EmailModel][create]")
        helpers.bulk(self.es, self.gendata(email_objects))

    def query_params_for_es(self, query_term, query_values):
        query_for_es = []
        for query_value in query_values:
            query_for_es.append({
                "match_phrase": {
                    query_term: query_value
                }
            })

        return query_for_es

    def filter_by_subject_and_content(self, subject_query, content_query):
        query = {
            "query": {
                "bool": {
                  "must": self.query_params_for_es("doc.subject", subject_query) +
                          self.query_params_for_es("doc.content", content_query)
                }
            }
        }

        return self.fetch_es_results(query)

    def filter_by_subject(self, subject_query):
        query = {
            "query": {
                "bool": {
                  "must": self.query_params_for_es("doc.subject", subject_query)
                }
            }
        }

        return self.fetch_es_results(query)

    def filter_by_content(self, content_query):
        query = {
            "query": {
                "bool": {
                  "must": self.query_params_for_es("doc.content", content_query)
                }
            }
        }

        return self.fetch_es_results(query)

    def fetch_es_results(self, query):
        result_obj = helpers.scan(self.es, query = query, index="index_email")
        results = []
        for res in result_obj:
            results.append({
                "id": res["_id"],
                "subject": res["_source"]["doc"]["subject"],
                "content": res["_source"]["doc"]["content"],
                "date": res["_source"]["doc"]["date"]
            })

        return results

    def fetch(self, query_object):
        self.logger.write("[email-model] query object: " + str(query_object))
        subject_query = []
        content_query = []
        results = []

        if query_object["q_subject"] and query_object["q_content"]:
            subject_query = query_object["q_subject"].split(",")
            content_query = query_object["q_content"].split(",")
            results += self.filter_by_subject_and_content(subject_query, content_query)

        elif query_object["q_subject"]:
            subject_query = query_object["q_subject"].split(",")
            results += self.filter_by_subject(subject_query)

        elif query_object["q_content"]:
            content_query = query_object["q_content"].split(",")
            results += self.filter_by_content(content_query)

        return results
