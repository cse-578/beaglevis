import operator
from sklearn.feature_extraction.text import TfidfVectorizer

class ContentModel():
    def __init__(self):
        self.vectorizer = TfidfVectorizer(analyzer='word', min_df = 0, stop_words = 'english')

    def tfidf(self, email_objects, content):
        corpus = []
        for email_obj in email_objects:
            corpus.append(email_obj[content])

        X = self.vectorizer.fit_transform(corpus)
        words = self.vectorizer.get_feature_names()
        dense = X.todense()
        episode = dense[0].tolist()[0]
        word_scores = [pair for pair in zip(range(0, len(episode)), episode) if pair[1] > 0]
        sorted_word_scores = sorted(word_scores, key=lambda t: t[1] * -1)

        result = {}
        for phrase, score in [(words[word_id], score) for (word_id, score) in sorted_word_scores]:
            result[phrase] = score

        return sorted(result.items(), key=operator.itemgetter(1), reverse=True)