class Util():
    def __init__(self):
        pass

    def query_params_for_es(self, email_ids):
        query = []
        for email_id in email_ids:
            query.append({ "match": { "_id": email_id } })

    def filter_by_email_id(self, email_ids):
        return {
          "query": {
            "bool": {
              "must" : {
                "bool" : {
                  "should" : self.query_params_for_es(email_ids)
                }
              }
            }
          }
        }