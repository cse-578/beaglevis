from beaglevis.model.cluster_model import ClusterModel
from beaglevis.model.content_model import ContentModel
from beaglevis.model.correspondent_model import CorrespondentModel
from beaglevis.model.email_model import EmailModel
from beaglevis.model.graph_model import GraphModel
from beaglevis.model.tag_model import TagModel
from beaglevis.model.timeline_model import TimelineModel

class SearchService():
    def __init__(self):
        self.cluster_model       = ClusterModel()
        self.content_model       = ContentModel()
        self.correspondent_model = CorrespondentModel()
        self.email_model         = EmailModel()
        self.graph_model         = GraphModel()
        self.tag_model           = TagModel()
        self.timeline_model      = TimelineModel()

        self.logger              = open("../logs/logger.log", "a")

    def search(self, queries):
        self.logger.write("[search-service] queries:" + str(queries))
        emails = self.email_model.fetch(queries)

        email_ids = []
        email_datetime = []
        email_subjects = []
        email_content = []

        for email in emails:
            email_ids.append(email["id"])
            email_datetime.append(email["date"])
            email_subjects.append({ "id": email["id"], "subject": email["subject"] })
            email_content.append({ "id": email["id"], "content": email["content"] })

        search_response = {
        "status": "success",
            "data": {
                "entitiesSubject": self.content_model.tfidf(email_subjects, "subject"),
                "entitiesContent": self.content_model.tfidf(email_content, "content"),
                "emails": emails,
                "correspondentEmail": self.correspondent_model.fetch(email_ids),
                "contactGraph": self.graph_model.fetch(email_ids),
                "clusters": self.cluster_model.fetch(emails),
                "tags": self.tag_model.fetch(email_ids),
                "emailTimeline": self.timeline_model.fetch(email_datetime)
            }
        }

        # return search_response
        return str(search_response)