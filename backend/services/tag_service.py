from beaglevis.model.tag_model import TagModel
class TagService():
    def __init__(self):
        self.tag_model = TagModel()
        self.logger    = open("../logs/logger.log", "a")

    def process(self, word, tag):
        self.logger.write("[tag-service] word: " + word + " tag: " + tag)
        processed_record = self.tag_model.create({
            "word": word,
            "tag": tag
            })

        return processed_record