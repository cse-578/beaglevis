from beaglevis.model.email_model import EmailModel

import json

class EmailService(object):
    def __init__(self):
        self.email_model = EmailModel()
        self.logger      = open("../logs/logger.log", "a")

    def load_data(self, file):
        self.logger.write("[EmailService][load_data] file loaded:" + file)
        with open(file ,encoding='utf-8', errors='ignore') as json_data:
            data = json.load(json_data, strict=False)
            self.email_model.create(data)