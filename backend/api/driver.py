from beaglevis.services.email_service import EmailService
from beaglevis.services.search_service import SearchService
from beaglevis.services.tag_service import TagService

from flask import Flask, request, send_file, jsonify
from werkzeug import secure_filename

import datetime
import logging
import os

DOWNLOAD_FOLDER = "../logs/"
UPLOAD_FOLDER = "../data/"

LOG_FILE = "logger" + str(datetime.datetime.now()) + ".log"

FILE_CONSTANTS = {
    "email_dataset": ["parsed_data1.json", "parsed_data2.json"]
}

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
app.config["DOWNLOAD_FOLDER"] = DOWNLOAD_FOLDER
file_handler = logging.FileHandler(DOWNLOAD_FOLDER + "logger.log")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(file_handler)

tag_service = TagService()
search_service = SearchService()
email_service = EmailService()

@app.route("/")
def home():
    app.logger.info("[api-home]")
    return "Home"

@app.route("/api/v1/search", methods=["GET"])
def search():
    global search_service
    app.logger.info("[api-search] request data: " + str(request.args))

    q_subject = request.args.get("subject")
    q_content = request.args.get("content")
    query_object = {
        "q_subject": q_subject,
        "q_content": q_content
    }

    return jsonify(search_service.search(query_object))

@app.route("/api/v1/upload", methods=["POST"])
def upload_dataset():
    global FILE_CONSTANTS
    if "files[]" not in request.files:
        return "Please upload the required dataset"
    uploaded_files = request.files.getlist("files[]")
    app.logger.info("[api-upload] files uploaded: " + str(uploaded_files))
    for file in uploaded_files:
        filename = os.path.join(app.config["UPLOAD_FOLDER"], secure_filename(file.filename))
        file.save(filename)
        if(file.filename in FILE_CONSTANTS["email_dataset"]):
            email_service.load_data(filename)

    # add service call to populate datastore and display results

    return "File uploaded successfully"

@app.route("/api/v1/tags", methods=["POST"])
def add_tags():
    global tag_service

    request_data = request.get_json()
    word = request_data["word"]
    tag = request_data["tag"]

    app.logger.info("[api-tags] request data: " + str(request_data))

    return jsonify(tag_service.process(word, tag))

@app.route("/api/v1/download", methods=["GET"])
def download(filename=DOWNLOAD_FOLDER + "logger.log"):
    app.logger.info("[api-download] download logs: " + filename)
    return send_file(filename, as_attachment=True)

if __name__ == "__main__":
    app.run(debug=True)
