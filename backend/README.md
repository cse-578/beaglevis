# BeagleVis

Lessons Learned Developing a Visual Analytics Solution for Investigative Analysis of Scamming Activities


**Requirements for backend app:**


1.  Python2.7+


2.  Packages to be installed -

`Flask==1.1.1`

`elasticsearch==7.0.5`

`sklearn==0.0`


3. Services to be set-up in order to run the flask app -

*  [Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html)
*  [Kibana (for accessing dashboard to Elasticsearch)](https://www.elastic.co/guide/en/kibana/current/install.html)